# PWA-OG

## About

![Hero section](./public/images/screenshot/hero_section.png)

```
Speed up your PWA web application development time by using this powerful SAAS to generate a ready-to-use web application manifest and splash screen images.

P.S.: You do not need an internet connection to use this tool.
P.S.S.: Don't thank me! It's FREE
```

[Try it here.](https://pwa-og.web.app)

## Screenshots

> Lighthouse audit

![Lighthouse audit](./public/images/screenshot/lighthouse_audit.png)

> Empty Form

![Empty Form](./public/images/screenshot/empty_manifest_form.png)

> Manifest Preview

![Manifest Preview](./public/images/screenshot/manifest_form.png)

> Generated Files

![Generated Files](./public/images/screenshot/generated_files.png)

> Small Screen

![Small Screen](./public/images/screenshot/small_screen.png)

## Build Setup

```
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production (Generate Static Project)
$ npm run build

# deploy
$ npm run deploy

# deploy using CI pipeline
$ npm run deploy-ci
```

## Resources

-   Iconify react logo npm package

/* eslint-disable no-unused-vars */

export declare global {
    interface Window {
        Prism: any;
    }

    namespace I {
        interface IAlertMessage {
            type?: string;
            title?: string;
            message?: string;
        }

        interface IManifest {
            name: string;
            short_name?: string;
            start_url?: string;
            display?: string;
            background_color?: string;
            theme_color?: string;
            description?: string;
            icons?: Array<IIcon>;
            related_applications?: IRelatedApplication[];
        }
        interface IIcon {
            src: string;
            sizes: string;
            type: string;
            purpose?: "maskable";
        }
        interface IRelatedApplication {
            platform: string;
            url: string;
            id?: string;
        }
    }
}

import React from "react";
import css from "styled-jsx/css";

import Page from "../components/Page";

export default function OfflinePage() {
    return (
        <>
            <Page title="Offline">
                <main id="offline_page" className="container">
                    <p>Your are not connected to the internet!</p>
                </main>
            </Page>

            <style jsx>{style}</style>
        </>
    );
}

const style = css`
    main {
        display: flex;
        justify-content: center;
        align-items: center;
        height: 100vh;
        padding-top: 100px;
    }
`;

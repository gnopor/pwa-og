import React, { useState } from "react";
import css from "styled-jsx/css";

import HeroSection from "../components/HeroSection";
import ManifestForm from "../components/forms/ManifestForm";
import CodePreview from "../components/CodePreview";
import Page from "../components/Page";

export default function HomePage() {
    const [manifest, setManifest] = useState<I.IManifest>();

    return (
        <>
            <Page title="Home">
                <main id="home_page" className="container">
                    <HeroSection />

                    <section id="manifest_generator">
                        <div>
                            <ManifestForm onUpdate={setManifest} />
                        </div>
                        <div className="preview_wrapper">
                            <CodePreview codeString={JSON.stringify(manifest, null, 2)} />
                        </div>
                    </section>
                </main>
            </Page>

            <style jsx>{style}</style>
        </>
    );
}

const style = css`
    main {
        display: flex;
        flex-direction: column;
        gap: 100px;
        padding-top: 100px;
    }

    #manifest_generator {
        display: flex;
        align-items: stretch;
        gap: 10px;
        z-index: 2;
    }
    #manifest_generator > div {
        flex: 1 0 50%;
        max-width: 50vw;
    }

    /* for tablet and smartphone */
    @media screen and (max-width: 768px) {
        #manifest_generator > div {
            flex: 1 0 auto;
            max-width: 100vw;
        }
        #manifest_generator .preview_wrapper {
            display: none;
        }
    }
`;

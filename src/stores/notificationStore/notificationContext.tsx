/* eslint-disable max-lines-per-function */
import React, { useState } from "react";

import NotificationBox from "../../components/NotificationBox";

interface INotificationContext {
    open: boolean;
    title: string;
    message: string;
    type: string;
    showNotification: (options: INotificationHandlerOptions) => void;
    closeNotification: () => void;
}
interface INotificationHandlerOptions {
    message: string;
    title?: string;
    type?: "error" | "succeed";
}
const NotificationContext = React.createContext({} as INotificationContext);

function useNotification() {
    const context = React.useContext(NotificationContext);
    if (context === undefined) {
        throw new Error("NotificationContext must be used within an NotificationProvider.");
    }
    return context;
}

function NotificationProvider({ children }: { children: React.ReactNode }) {
    const [open, setOpen] = useState(false);
    const [title, setTitle] = useState("");
    const [message, setMessage] = useState("");
    const [type, setType] = useState("success");

    const showNotification = (options: INotificationHandlerOptions) => {
        const { message = "", title = "", type = "success" } = options;
        setTitle(title || "Notification");
        setMessage(message);
        setType(type);
        setOpen(true);
    };
    const closeNotification = () => {
        setOpen(false);
    };

    const value = {
        open,
        title,
        message,
        type,
        showNotification,
        closeNotification
    };

    return (
        <NotificationContext.Provider value={value}>
            {children}
            <NotificationBox {...value} />
        </NotificationContext.Provider>
    );
}

export { NotificationProvider, useNotification };

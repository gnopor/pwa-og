import React, { useEffect, useRef } from "react";
import css from "styled-jsx/css";

export default function VerticalPageHorizontalProgess({
    mainColor = "var(--primary)",
    placeHolderColor = "var(--light-red)"
}) {
    const progressRef = useRef<HTMLDivElement>(null);

    useEffect(() => {
        runScrollWatcher();
    }, []);

    const runScrollWatcher = () => {
        window.addEventListener("scroll", () => {
            const maxHeight = document.body.scrollHeight - window.innerHeight;
            const currentHeight = window.scrollY;

            const progress = (currentHeight / maxHeight) * 100;
            progressRef.current?.style.setProperty("--progress", `${progress}%`);
        });
    };

    return (
        <>
            <div className="progress_wrapper">
                <div
                    ref={progressRef}
                    className="progress"
                    style={
                        {
                            "--progress": "0%",
                            "--bg-place-holder": placeHolderColor,
                            "--bg-current-progress": mainColor
                        } as React.CSSProperties
                    }
                ></div>
            </div>

            <style jsx>{style}</style>
        </>
    );
}

const style = css`
    .progress_wrapper {
        position: fixed;
        top: -1px;
        width: 100vw;
        height: 7px;
        z-index: 4;
    }
    .progress {
        position: relative;
        background: var(--bg-place-holder);
        width: inherit;
        height: inherit;
    }

    .progress::before {
        content: "";
        position: absolute;
        background-color: var(--bg-current-progress);
        height: inherit;
        width: var(--progress);
    }
`;

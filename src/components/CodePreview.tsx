import React, { useEffect, useRef } from "react";
import css from "styled-jsx/css";

interface IProps {
    codeString: string;
}

export default function CodePreview({ codeString }: IProps) {
    const preRef = useRef<HTMLPreElement>(null);

    useEffect(() => {
        if (!codeString) return;

        const prism = window?.Prism || {};
        prism?.highlightAllUnder(preRef.current);
    }, [codeString]);

    return (
        <>
            <div className="code_preview_wrapper">
                <pre ref={preRef}>
                    <code className="language-json">{codeString}</code>
                </pre>
            </div>

            <style jsx>{style}</style>
        </>
    );
}

const style = css`
    .code_preview_wrapper {
        height: 100%;
    }

    .code_preview_wrapper pre {
        position: relative;
        height: 100%;
    }
    .code_preview_wrapper pre::-webkit-scrollbar {
        height: 15px;
    }
    .code_preview_wrapper pre::-webkit-scrollbar-track {
        background: rgba(241, 241, 241, 0.3);
    }
    .code_preview_wrapper pre::-webkit-scrollbar-thumb {
        background: #555;
    }

    .code_preview_wrapper code {
        position: absolute;
        max-width: 100%;
    }
`;

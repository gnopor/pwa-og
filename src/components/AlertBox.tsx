import React, { useEffect } from "react";

import { MAX_ALERT_VISIBILITY_TIME } from "../constants";

interface IProps {
    title?: string;
    message?: string;
    onClose?: Function;
    type?: string;
}

export default function AlertBox({ title, message, onClose, type }: IProps) {
    useEffect(() => {
        message && handleAutoClose();
    }, [message]);

    const onAlertClose = () => {
        onClose?.();
    };

    const handleAutoClose = () => {
        setTimeout(() => onClose?.(), MAX_ALERT_VISIBILITY_TIME);
    };

    return (
        <div className="alert" data-type={type}>
            <div className="alert_header">
                <span className="alert_title">{title?.slice(0, 50)}</span>
                <button type="button" onClick={onAlertClose}>
                    x
                </button>
            </div>
            <div className="alert_body">
                <p>{message?.slice(0, 100)}</p>
            </div>
        </div>
    );
}

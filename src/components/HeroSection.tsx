/* eslint-disable react/no-unescaped-entities */
import React from "react";
import css from "styled-jsx/css";
import Link from "./Link";
import Logo from "./Logo";

export default function HeroSection() {
    return (
        <>
            <section id="hero_section">
                <figure>
                    <Link href="?#manifest_generator">
                        <Logo color="var(--primary)" />
                    </Link>
                </figure>

                <p>
                    <span>Hello!🖐️</span>, Welcome aboard <br />
                    Speed up your PWA web application development time by using this powerful SAAS
                    to generate a ready-to-use web application manifest and splash screen images.
                    <br />
                    <br />
                    P.S.: You do not need an internet connection to use this tool.
                    <br />
                    P.S.S.: Don't thank me! <b>It's FREE</b>
                </p>
            </section>

            <style jsx>{style}</style>
        </>
    );
}

const style = css`
    #hero_section {
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        gap: 20px;
        height: 100vh;
        max-width: 100vw;
        overflow: hidden;
    }

    figure {
        transform: scale(5);
        margin-bottom: 100px;
    }
    figure:hover {
        filter: grayscale(0.5);
    }

    p {
        text-align: center;
        line-height: 1.5;
        max-width: 700px;
    }
    p span {
        font-size: 3em;
    }
    p::first-letter {
        color: tomato;
        font-size: 3rem;
        font-weight: bold;
    }

    /* for tablet and smartphone */
    @media screen and (max-width: 768px) {
        figure {
            transform: scale(4);
        }
    }
`;

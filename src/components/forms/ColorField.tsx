import React, { useEffect, useState } from "react";
import css from "styled-jsx/css";
import Helpers from "../../utilities/helpers/helpers";

interface IProps extends React.InputHTMLAttributes<HTMLInputElement> {
    label?: string;
    color?: string;
    onColorChange?: Function;
}

export default function ColorField({
    id,
    label,
    required,
    placeholder,
    color,
    onColorChange,
    ...rest
}: IProps) {
    const [colorPicker, setColorPicker] = useState(color);
    const [colorInput, setColorInput] = useState("");

    useEffect(() => {
        setColorPicker(color);
        !color && setColorInput("");
    }, [color]);

    const handleColorPickerChange = (value: string) => {
        setColorPicker(value);
        onColorChange?.(value);
        value !== colorInput && setColorInput(value);
    };

    const handleColorInputChange = (value: string) => {
        let newColor = value.trim();
        setColorInput(value);

        if (isRGB(newColor)) {
            const result = newColor.match(/[0-9]+,[0-9]+,[0-9]+/);
            const [r, g, b] = String(result).split(",");

            newColor = Helpers.rgb2hex(+r, +g, +b);
        }

        if (!isHex(newColor)) return;

        setColorPicker(newColor);
        onColorChange?.(newColor);
    };

    const isRGB = (str: string) => {
        const regex = /^rgb\([0-9]+,[0-9]+,[0-9]+\)$/gi;
        return regex.test(str);
    };
    const isHex = (str: string) => {
        const regex = /^#[0-9,a-f]{6}$/gi;
        return regex.test(str);
    };

    return (
        <>
            <div className="form_control">
                <label
                    data-required-symbol="*"
                    htmlFor={id}
                    className={`form_label ${required ? "required" : ""}`}
                >
                    {label}
                </label>

                <div className="input_wrapper">
                    <input
                        required={required}
                        type="color"
                        value={colorPicker || "#000000"}
                        onChange={(e) => handleColorPickerChange(e.target.value)}
                        {...rest}
                    />
                    <input
                        className="form_input"
                        id={id}
                        value={colorInput}
                        placeholder={placeholder}
                        onChange={(e) => handleColorInputChange(e.target.value)}
                    />
                </div>
            </div>

            <style jsx>{style}</style>
        </>
    );
}

const style = css`
    .input_wrapper {
        display: flex;
        align-items: center;
        gap: 10px;
    }
    .input_wrapper input[type="color"] {
        border: none;
        height: 35px;
        width: 35px;
        cursor: pointer;
    }
`;

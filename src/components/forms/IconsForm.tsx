import React from "react";
import css from "styled-jsx/css";

import { useNotification } from "../../stores/notificationStore/notificationContext";
import { PWA_ICONS_SIZES } from "../../constants";
import FileHelpers from "../../utilities/helpers/file.helpers";

const fileSuffix = "splashScreenIcon";
interface IProps {
    saveIcons: (icons: File[]) => void;
    file?: File;
    setFile: (f?: File) => void;
}

export default function IConsForm({ file, setFile, saveIcons }: IProps) {
    const { showNotification } = useNotification();

    const handleInputChange = async (files: FileList) => {
        try {
            const newFile = FileHelpers.onFileSelect(files[0]);
            if (!newFile) return;
            setFile(newFile);

            setTimeout(async () => {
                const newIcons = await Promise.all(
                    PWA_ICONS_SIZES.map((size) => loadImage(newFile, size))
                );

                saveIcons(newIcons);
            }, 0);
        } catch (error: any) {
            console.error(error);
            showNotification({ message: error?.message || "", type: "error" });
        }
    };
    const loadImage = async (image: File, size: number) => {
        const selector = `#${fileSuffix}${size}`;
        const fileName = `${fileSuffix}${size}x${size}.png`;

        await FileHelpers.showImage(image, selector);
        const newImage = await FileHelpers.resizeImage(selector, {
            width: size,
            height: size,
            fileName
        });
        return newImage;
    };

    const handleRemoveImages = () => {
        setFile(undefined);
        saveIcons([]);
    };

    return (
        <>
            <div className="icons_form">
                <header className="icons_form_header">
                    <div className="image_loader_wrapper">
                        <input
                            style={{ display: "none" }}
                            accept="image/*"
                            id="image_loader-file-button"
                            type="file"
                            onChange={(e) => handleInputChange(e.target?.files as FileList)}
                        />

                        {!file?.size ? (
                            <button type="button" className="btn btn--block">
                                <label
                                    style={{ width: "100%", display: "block" }}
                                    htmlFor="image_loader-file-button"
                                >
                                    + Add Icons
                                </label>
                            </button>
                        ) : (
                            <button
                                type="button"
                                className="btn btn--block"
                                onClick={handleRemoveImages}
                            >
                                <span>- Remove Icons</span>
                            </button>
                        )}
                    </div>
                </header>

                <div className="icons_wrappers">
                    {file?.size &&
                        PWA_ICONS_SIZES.map((size, i) => (
                            <article key={i}>
                                <span>{`${size}x${size}`}</span>
                                <img
                                    id={`${fileSuffix}${size}`}
                                    src=""
                                    style={{ "--size": `${size}px` } as React.CSSProperties}
                                />
                            </article>
                        ))}
                </div>
            </div>

            <style jsx>{style}</style>
        </>
    );
}

const style = css`
    .icons_form {
        display: flex;
        flex-direction: column;
        gap: 10px;
    }

    .icons_form_header {
        display: flex;
        justify-content: center;
    }
    .icons_form_header .image_loader_wrapper {
        width: 100%;
    }

    .icons_wrappers {
        display: flex;
        flex-wrap: wrap;
        justify-content: space-between;
        gap: 10px;
    }
    .icons_wrappers article {
        flex: 1 0 30%;
        display: flex;
        flex-direction: column;
        align-items: center;
        width: 100px;
    }
    .icons_wrappers span {
        background-color: var(--primary);
        color: var(--white);
        font-size: 0.8em;
        font-weight: bold;
        border-radius: 10px;
        padding: 5px;
        margin-bottom: 5px;
    }
    .icons_wrappers img {
        height: var(--size);
        width: var(--size);
        max-width: 300px;
        max-height: 300px;
    }

    /* for tablet and smartphone */
    @media screen and (max-width: 768px) {
        .icons_wrappers img {
            display: none;
        }
    }
`;

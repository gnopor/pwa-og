/* eslint-disable max-lines-per-function */
import React, { useEffect, useState } from "react";
import css from "styled-jsx/css";

import AlertBox from "../AlertBox";
import ColorField from "./ColorField";
import IConsForm from "./IconsForm";
import RelatedApplicationForm from "./RelatedApplicationsForm";
import TextField from "./TextField";
import { PWA_DISPLAY_MODE } from "../../constants";
import manifestFactory from "../../utilities/factories/manifest.factory";
import ValidationHelpers from "../../utilities/helpers/validation.helpers";
import FileHelpers from "../../utilities/helpers/file.helpers";
import Button from "../Button";

const MANIFEST_LOCAL_STORAGE_KEY = "x-manifest";

interface IProps {
    manifest?: I.IManifest;
    onUpdate: Function;
}

export default function ManifestForm({ manifest, onUpdate }: IProps) {
    const [formValues, setFormValues] = useState<I.IManifest>(manifest || manifestFactory.create());
    const [isOldFormLoaded, setIsOldFormLoaded] = useState(!!manifest);

    const [file, setFile] = useState<File>();
    const [icons, setIcons] = useState<File[]>();

    const [loading, setLoading] = useState(false);
    const [alertMessage, setAlertMessage] = useState<I.IAlertMessage>({ type: "", message: "" });

    useEffect(() => {
        if (!isOldFormLoaded) {
            loadOldFormValue();
        } else {
            localStorage.setItem(MANIFEST_LOCAL_STORAGE_KEY, JSON.stringify(formValues));
        }

        onUpdate(formValues);
    }, [formValues]);

    const loadOldFormValue = () => {
        const oldForm = localStorage.getItem(MANIFEST_LOCAL_STORAGE_KEY);
        if (!oldForm) return;

        const manifest = manifestFactory.create(JSON.parse(oldForm));
        setFormValues(manifest);
        setIsOldFormLoaded(true);
    };

    const handleSubmitForm = async () => {
        try {
            setLoading(true);
            const data = await ValidationHelpers.isManifestFormValid(formValues);

            FileHelpers.downloadJSON(JSON.stringify(data, null, 2), { fileName: "manifest.json" });
            for (const icon of icons || []) {
                FileHelpers.downloadImage(icon, { fileName: icon?.name || "" });
            }

            setAlertMessage({ message: "Your PWA files have been generated." });
            handleResetForm();
        } catch (error: any) {
            setAlertMessage({ message: error?.message || "", type: "error" });
        } finally {
            setLoading(false);
        }
    };
    const handleResetForm = () => {
        localStorage.setItem(MANIFEST_LOCAL_STORAGE_KEY, "");
        setFormValues(manifestFactory.create());
        setFile(undefined);
        setIcons(undefined);
    };

    const handleIconsUpdate = (images: File[]) => {
        const newIconsList = [];
        for (const img of images) {
            const size = img.name.match(/(?<size>[0-9]+)\.png/)?.groups?.size || "";
            if (!size) continue;

            const icon: I.IIcon = {
                src: `images/icons/${img.name}`,
                sizes: `${size}x${size}`,
                type: "image/png"
            };
            if (+size === 96) icon.purpose = "maskable";

            newIconsList.push(icon);
        }

        setIcons(images);
        setFormValues({ ...formValues, icons: newIconsList });
    };

    return (
        <>
            <form>
                <div>
                    <TextField
                        label="Name"
                        id="name_input"
                        required
                        value={formValues?.name || ""}
                        onChange={(e) => setFormValues({ ...formValues, name: e.target.value })}
                    />
                </div>

                <div>
                    <TextField
                        label="Short Name"
                        id="short_name_input"
                        value={formValues?.short_name || ""}
                        onChange={(e) =>
                            setFormValues({ ...formValues, short_name: e.target.value })
                        }
                    />
                </div>

                <div>
                    <TextField
                        label="Start URL"
                        id="start_url_input"
                        value={formValues?.start_url || ""}
                        onChange={(e) =>
                            setFormValues({ ...formValues, start_url: e.target.value.trim() })
                        }
                    />
                </div>

                <div>
                    <div className="form_control">
                        <label htmlFor="description_input" className={"form_label"}>
                            Description
                        </label>
                        <textarea
                            className="form_input"
                            id="description_input"
                            value={formValues?.description || ""}
                            onChange={(e) =>
                                setFormValues({ ...formValues, description: e.target.value })
                            }
                        />
                    </div>
                </div>

                <div>
                    <div className="form_control">
                        <label htmlFor="display_input" className="form_label">
                            Display
                        </label>
                        <select
                            id="display_input"
                            className="form_input"
                            value={formValues?.display || ""}
                            onChange={(e) =>
                                setFormValues({ ...formValues, display: e.target.value })
                            }
                        >
                            <option></option>
                            {PWA_DISPLAY_MODE.map((mode, i) => (
                                <option value={mode} key={i}>
                                    {mode}
                                </option>
                            ))}
                        </select>
                    </div>
                </div>

                <div>
                    <ColorField
                        id="background_color_input"
                        label="Background Color"
                        color={formValues?.background_color || ""}
                        onColorChange={(v: string) =>
                            setFormValues({ ...formValues, background_color: v })
                        }
                        placeholder="#000000 or rgb(0,0,0)"
                    />
                </div>

                <div>
                    <ColorField
                        id="theme_color_input"
                        label="Theme Color"
                        color={formValues?.theme_color || ""}
                        onColorChange={(v: string) =>
                            setFormValues({ ...formValues, theme_color: v })
                        }
                        placeholder="#000000 or rgb(0,0,0)"
                    />
                </div>

                <hr className="divider" />

                <div>
                    <RelatedApplicationForm
                        label="Related Applications"
                        list={formValues?.related_applications || []}
                        saveList={(v: I.IRelatedApplication[]) =>
                            setFormValues({ ...formValues, related_applications: v })
                        }
                    />
                </div>

                <hr className="divider" />

                <div className="image_wrapper">
                    <IConsForm file={file} setFile={setFile} saveIcons={handleIconsUpdate} />
                </div>

                <hr className="divider" />

                <div className="form_actions">
                    <Button onClick={handleResetForm} disabled={loading} type="button" block>
                        Cancel
                    </Button>
                    <Button
                        onClick={handleSubmitForm}
                        disabled={loading}
                        type="button"
                        block
                        variant="primary"
                    >
                        Download
                    </Button>
                </div>

                <div>
                    {alertMessage.message && (
                        <AlertBox
                            message={alertMessage.message}
                            type={alertMessage.type}
                            onClose={() => setAlertMessage({})}
                        />
                    )}
                </div>
            </form>

            <style jsx>{style}</style>
        </>
    );
}

const style = css`
    form {
        display: flex;
        flex-direction: column;
        gap: 20px;
    }

    .form_actions {
        display: flex;
        gap: 20px;
    }

    /* for tablet and smartphone */
    @media screen and (max-width: 768px) {
        .form_actions {
            flex-direction: column;
        }
    }
`;

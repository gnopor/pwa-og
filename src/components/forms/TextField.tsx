import React from "react";

interface IProps extends React.InputHTMLAttributes<HTMLInputElement> {
    label?: string;
}

const TextField = React.forwardRef<HTMLInputElement, IProps>(
    ({ id, label, required, ...rest }, ref) => {
        return (
            <div className="form_control">
                <label
                    data-required-symbol="*"
                    htmlFor={id}
                    className={`form_label ${required ? "required" : ""}`}
                >
                    {label}
                </label>
                <input className="form_input" id={id} required={required} {...rest} />
            </div>
        );
    }
);
TextField.displayName = "TextField";

export default TextField;

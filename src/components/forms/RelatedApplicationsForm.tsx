/* eslint-disable max-lines-per-function */
import React, { useState } from "react";
import css from "styled-jsx/css";

import TextField from "./TextField";
import { PWA_RELATED_APPLICATION_PLATFORM } from "../../constants";

const PLATFORMS = PWA_RELATED_APPLICATION_PLATFORM;

interface IProps {
    label?: string;
    list: I.IRelatedApplication[];
    saveList: Function;
}

export default function RelatedApplicationForm({ label, list, saveList }: IProps) {
    const [relatedApp, setRelatedApp] = useState<I.IRelatedApplication>({
        platform: "",
        url: ""
    });
    const [showBody, setShowBody] = useState(false);

    const handleAddRelatedApp = () => {
        saveList([...list, relatedApp]);
        setRelatedApp({ platform: "", url: "" });
    };

    const handleRemoveRelatedApp = (index: number) => {
        const newList = list.filter((_item, i) => i !== index);
        saveList(newList);
    };

    const handleToggle = () => {
        setShowBody(!showBody);
    };

    return (
        <>
            <div data-active={showBody} className="accordion">
                <div className="accordion_header">
                    <button type="button" className="btn btn--block" onClick={() => handleToggle()}>
                        <span>{`${label} (${list.length})`}</span>
                        <span className="anchor">^</span>
                    </button>
                </div>

                <div className="accordion_body">
                    <div className="accordion_body">
                        <ul>
                            {list.map((app, i) => (
                                <li key={i}>
                                    <article>
                                        <div>
                                            <h5>Platform: {app.platform} </h5>
                                            <button
                                                type="button"
                                                onClick={() => handleRemoveRelatedApp(i)}
                                            >
                                                x
                                            </button>
                                        </div>
                                        <span>URL: {app.url} </span>
                                        {app.id && <span>Id: {app.id} </span>}
                                    </article>
                                </li>
                            ))}
                        </ul>

                        <div className="form_wrapper">
                            <div>
                                <div className="form_control">
                                    <label
                                        data-required-symbol="*"
                                        htmlFor="display_input"
                                        className="form_label required"
                                    >
                                        Display
                                    </label>
                                    <select
                                        value={relatedApp?.platform || ""}
                                        onChange={(e) =>
                                            setRelatedApp({
                                                ...relatedApp,
                                                platform: e.target.value
                                            })
                                        }
                                        id="display_input"
                                        className="form_input"
                                    >
                                        <option></option>
                                        {PLATFORMS.map((mode: string, i) => (
                                            <option value={mode} key={i}>
                                                {mode}
                                            </option>
                                        ))}
                                    </select>
                                </div>
                            </div>
                            <div>
                                <TextField
                                    label="URL"
                                    value={relatedApp?.url || ""}
                                    onChange={(e) =>
                                        setRelatedApp({
                                            ...relatedApp,
                                            url: e.target.value
                                        })
                                    }
                                    required
                                />
                            </div>
                            <div>
                                <TextField
                                    label="Id"
                                    value={relatedApp?.id || ""}
                                    onChange={(e) =>
                                        setRelatedApp({
                                            ...relatedApp,
                                            id: e.target.value
                                        })
                                    }
                                />
                            </div>
                            <div>
                                <button
                                    type="button"
                                    disabled={!(relatedApp?.platform && relatedApp.url)}
                                    className="btn btn--block"
                                    onClick={() => handleAddRelatedApp()}
                                >
                                    + Add
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <style jsx>{style}</style>
        </>
    );
}

const style = css`
    ul {
        display: flex;
        flex-direction: column;
        list-style: none;
    }
    ul li {
        margin: 10px 0px;
    }
    ul article {
        display: flex;
        flex-direction: column;
        background-color: var(--light-red);
        padding: 5px;
    }
    ul article div {
        display: flex;
        justify-content: space-between;
    }
    ul article button {
        color: inherit;
        background-color: inherit;
        border: none;
        margin-left: 10px;
        width: 20px;
        height: 20px;
    }
    ul article button:hover {
        border: 1px solid;
    }

    .form_wrapper {
        display: flex;
        flex-direction: column;
        gap: 10px;
    }
`;

import React from "react";
import css from "styled-jsx/css";

export default function Logo({ color = "#fff" }) {
    return (
        <>
            <figure style={{ "--color": color } as React.CSSProperties} id="icon">
                <span>pwa-og</span>
                <span>Offline Generator</span>
            </figure>

            <style jsx>{style}</style>
        </>
    );
}

const style = css`
    #icon {
        position: relative;
        margin: 20px;
        width: fit-content;
    }
    #icon::before {
        content: "";
        position: absolute;
        top: -0.5em;
        left: -0.5em;
        border: 0.3em solid var(--color);
        border-width: 0.3em 0px 0px 0.3em;
        height: 0.3em;
        width: 0.3em;
    }

    #icon span {
        display: block;
    }
    #icon span:first-child {
        text-transform: uppercase;
        color: transparent;
        font-size: 1em;
        -webkit-text-stroke: 2px var(--color);
    }
    #icon span:last-child {
        color: var(--color);
        font-size: 0.6em;
        margin-top: -5px;
    }
`;

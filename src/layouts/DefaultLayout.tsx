import React from "react";

import Footer from "../components/Footer";
import Header from "../components/Header";
import VerticalPageHorizontalProgess from "../components/VerticalPageHorizontalProgess";

export default function DefaultLayout({ children }: any) {
    return (
        <>
            <VerticalPageHorizontalProgess />

            <Header />
            {children}
            <Footer />
        </>
    );
}

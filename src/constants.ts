export const APP_NAME = "PWA-OG";

export const WEBAPP_DOMAIN = "https://pwa-og.web.app";

export const WEBAPP_DESCRIPTION = `
PWA-OG is an offline PWA manifest generator that you can use for free to speed up your PWA web application development time. 
It's used to generate a ready-to-use web application manifest and splash screen images.
    `;

export const APP_REPOSITORY = "https://gitlab.com/gnopor/pwa-og";

export const LINKEDIN_PROFILE_URI = "https://www.linkedin.com/in/tayou-blaise-9b3a4b191/";

export const MAX_ALERT_VISIBILITY_TIME = 10000;

export const PWA_DISPLAY_MODE = ["fullscreen", "standalone", "minimal-ui", "browser"];

export const PWA_RELATED_APPLICATION_PLATFORM = [
    "amazone",
    "chrome_web_store",
    "f-droid",
    "itunes",
    "play",
    "webapp",
    "windows"
];

export const PWA_ICONS_SIZES = [48, 72, 96, 144, 168, 192, 384, 512];

// Environment variables configs
export const API_BASE_URI = process.env.NEXT_PUBLIC_API_BASE_URI || "";

export const APP_PUBLIC_URI = process.env.NEXT_PUBLIC_APP_PUBLIC_URI || "";

class ManifestFactory {
    #manifest: I.IManifest;

    constructor() {
        this.#manifest = { name: "" };
    }

    create(options?: { [key: string]: any }) {
        const newManifest: I.IManifest = { ...this.#manifest };

        newManifest.name = options?.name || "";
        newManifest.short_name = options?.short_name || "";
        newManifest.start_url = options?.start_url || "";
        newManifest.display = options?.display || "";
        newManifest.background_color = options?.background_color || "";
        newManifest.theme_color = options?.theme_color || "";
        newManifest.description = options?.description || "";
        newManifest.icons = options?.icons || [];
        newManifest.related_applications = options?.related_applications || [];

        return newManifest;
    }
}

export default new ManifestFactory();

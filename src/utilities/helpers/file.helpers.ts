export default class FileHelpers {
    static downloadJSON(data: string, options: { fileName?: string; onSucceed?: Function }) {
        const { fileName = "", onSucceed } = options;

        const type = "application/json";
        const content = data;

        const link = document.createElement("a");
        link.setAttribute("href", `data:${type};charset=utf-8, ${encodeURIComponent(content)}`);
        link.setAttribute("download", fileName);
        link.style.display = "none";

        document.body.append(link);
        link.click();
        link.remove();
        onSucceed?.();
    }

    static downloadImage(data: File, options: { fileName?: string; onSucceed?: Function }) {
        const { fileName = "", onSucceed } = options;

        // const type = data.type;
        const content = URL.createObjectURL(data);

        const link = document.createElement("a");
        // link.setAttribute("href", `data:${type};charset=utf-8, ${encodeURIComponent(content)}`);
        link.setAttribute("href", content);
        link.setAttribute("download", fileName);
        link.style.display = "none";

        document.body.append(link);
        link.click();
        link.remove();

        URL.revokeObjectURL(content);
        onSucceed?.();
    }

    // image --------------------------------------------
    static onFileSelect(file: File, sizeLimit = 2000000) {
        try {
            if (!file) {
                return;
            }

            if (file.size > sizeLimit) {
                throw new Error("Your file must be less that 2Mb.");
            }

            return file;
        } catch (error) {
            throw error;
        }
    }

    static showImage(image: File, imageSelector: string) {
        return new Promise((resolve, reject) => {
            const img = document.querySelector(imageSelector) as HTMLImageElement;
            if (!img) {
                reject(new Error("There is an error with imageSelector."));
            }

            img.src = URL.createObjectURL(image);
            img.onload = () => {
                URL.revokeObjectURL(img.src);
                resolve(image);
            };
        });
    }

    static resizeImage(
        imageSelector: string,
        options: { width: number; height: number; fileName?: string }
    ): Promise<File> {
        return new Promise((resolve, reject) => {
            const { width, height, fileName = "" } = options;

            const elem = document.createElement("canvas");
            elem.width = width;
            elem.height = height;

            const ctx = elem.getContext("2d");
            const img = document.querySelector(imageSelector) as HTMLImageElement;
            if (!img) {
                reject(new Error("There is an error with imageSelector."));
            }

            ctx?.drawImage(img, 0, 0, width, height);
            ctx?.canvas.toBlob((blob) => {
                const file = new File([blob || ""], fileName, {
                    type: "image/png",
                    lastModified: Date.now()
                });
                resolve(file);
            });
        });
    }
}

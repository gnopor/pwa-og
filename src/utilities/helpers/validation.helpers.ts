export default class ValidationHelpers {
    static isManifestFormValid<T extends I.IManifest>(formValues: T) {
        const data: { [key: string]: any } = { ...formValues };
        const fields = ["name"];

        return new Promise<T>((resolve, reject) => {
            for (const field of fields) {
                const value = String(data[field] || "").trim();

                if (!value) {
                    reject(new Error(`There is an error with the field ${field}.`));
                }
                data[field] = value;
            }

            resolve(data as T);
        });
    }
}

/**
 *  You should regularly refactor this file and move functions
 *  that have outgrown the file into their own proper modules(services),
 *  otherwise there's a tendency that you'll just leave the functions
 *  there growing related functions without ever graduating into a proper module.
 */

export default class Helpers {
    static onConnectionStateChange(callback: (connected: boolean) => void) {
        callback(window.navigator.onLine);

        window.addEventListener("online", () => {
            callback(true);
        });
        window.addEventListener("offline", () => {
            callback(false);
        });
    }

    static rgb2hex(r: number, g: number, b: number) {
        const hr = this.getHexUnit(r);
        const hg = this.getHexUnit(g);
        const hb = this.getHexUnit(b);

        return `#${hr}${hg}${hb}`;
    }
    static getHexUnit(unitColor: number) {
        return Math.max(0, Math.min(255, Math.round(unitColor)))
            .toString(16)
            .padStart(2, "0");
    }

    static hex2rgb(hex: string) {
        const r = parseInt(hex.slice(1, 3), 16);
        const g = parseInt(hex.slice(1, 3), 16);
        const b = parseInt(hex.slice(1, 3), 16);

        return [r, g, b];
    }
}

interface IParams {
    [key: string]: string;
}

export default class PathHelpers {
    static onPathChange(callback: (newPath?: string) => void) {
        const element = window.document;
        let previousPath = window.location.pathname;

        callback?.(previousPath);

        const observer = new MutationObserver((mutationRecords) => {
            mutationRecords.every(() => {
                if (previousPath !== window.location.pathname) {
                    previousPath = window.location.pathname;
                    callback?.(previousPath);
                    return false;
                }
                return true;
            });
        });

        const options = { subtree: true, attributes: true };
        observer.observe(element, options);
    }

    static parseParameters(params?: IParams) {
        if (!params) return "";

        let parameters = "?";
        for (const [key, value] of Object.entries(params)) {
            parameters += `${key}=${value}&`;
        }

        return parameters.slice(0, parameters.length - 1);
    }

    // -----------------------------

    static error404Path() {
        return "/404/";
    }
    static homePagePath(languageCode?: string) {
        return "/home/";
    }
}
